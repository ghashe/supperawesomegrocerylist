package com.revature.models;

public class Item {
    Integer id;
    String name;
    Boolean inCart;
    Integer listFk;

    public Item(String name, Integer listFk) {
        this.name = name;
        this.listFk = listFk;
    }

    public Item(Integer id, String name, Boolean inCart, Integer listFk) {
        this.id = id;
        this.name = name;
        this.inCart = inCart;
        this.listFk = listFk;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getInCart() {
        return inCart;
    }

    public void setInCart(Boolean inCart) {
        this.inCart = inCart;
    }

    public Integer getListFk() {
        return listFk;
    }

    public void setListFk(Integer listFk) {
        this.listFk = listFk;
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inCart=" + inCart +
                ", listFk=" + listFk +
                '}';
    }
}
