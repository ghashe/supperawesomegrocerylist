package com.revature.models;

public class GroceryList {
    Integer id;
    String name;
    Integer userFk;

    public GroceryList() {
    }

    public GroceryList(String name, Integer userFk) {
        this.name = name;
        this.userFk = userFk;
    }

    public GroceryList(Integer id, String name, Integer userFk) {
        this.id = id;
        this.name = name;
        this.userFk = userFk;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserFk() {
        return userFk;
    }

    public void setUserFk(Integer userFk) {
        this.userFk = userFk;
    }

    @Override
    public String toString() {
        return "GroceryList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userFk=" + userFk +
                '}';
    }
}
