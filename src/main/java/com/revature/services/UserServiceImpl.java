package com.revature.services;

import com.revature.dao.UserDao;
import com.revature.dao.UserDaoImpl;
import com.revature.models.User;

public class UserServiceImpl implements UserService {
    UserDao userDao;

    public UserServiceImpl() {
        userDao = UserDaoImpl.getInstance();
    }

    @Override
    public Boolean register(User user) {
        // Check if the user exists in the system
        User tempUser = userDao.getOneUser(user.getUsername());

        if (tempUser != null)
            return false;

        userDao.insertUser(user);
        return true;
    }


    @Override
    public User login(User user) {
        // Check if the user exists in the system

        User tempUser = userDao.getOneUser(user.getUsername());
        if (tempUser == null)
            return null;

        //Check if password is correct
        if (!tempUser.getPassword().equals(user.getPassword()))
            return null;
        return tempUser;
    }
}