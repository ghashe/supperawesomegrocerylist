package com.revature.services;

import com.revature.models.Item;

import java.util.List;

public interface ItemService {
    void createItem(Item item);
    List<Item> getAllItemsGivenListId(Integer listId);
    void deleteItem(Integer itemId);
    void markItemInCart(Integer itemId);
}
