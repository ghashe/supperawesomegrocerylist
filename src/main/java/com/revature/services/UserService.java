package com.revature.services;

import com.revature.models.User;

public interface UserService{
    Boolean register(User user);
    User login(User user);
}
