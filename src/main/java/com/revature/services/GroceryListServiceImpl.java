package com.revature.services;

import com.revature.dao.GroceryListDao;
import com.revature.dao.GroceryListDaoImpl;
import com.revature.dao.ItemDao;
import com.revature.models.GroceryList;

import java.util.List;

public class GroceryListServiceImpl implements  GroceryListService{
    GroceryListDao groceryListDao;

    public GroceryListServiceImpl(){
        groceryListDao = GroceryListDaoImpl.getInstance();
    }

    @Override
    public void deleteListGivenListId(Integer listId) {
        groceryListDao.deleteListGivenListId(listId);
    }

    @Override
    public List<GroceryList> getAllListsGivenUserId(Integer userId) {
       return groceryListDao.getAllListsGivenUserId(userId);
    }

    @Override
    public void createList(GroceryList list) {
        groceryListDao.createList(list);
    }
}
