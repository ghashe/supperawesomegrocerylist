package com.revature.services;

import com.revature.models.GroceryList;

import java.util.List;

public interface GroceryListService {
    void deleteListGivenListId(Integer listId);
    List<GroceryList> getAllListsGivenUserId(Integer userId);
    void createList(GroceryList list);
}
