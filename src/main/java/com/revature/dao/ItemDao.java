package com.revature.dao;

import com.revature.models.Item;

import java.util.List;

public interface ItemDao {
    void createItem(Item item);
    List<Item> getAllItemsGivenListId(Integer listId);
    void deleteItem(Integer itemId);
    void markItemInCart(Integer itemId);
}
