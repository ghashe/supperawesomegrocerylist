package com.revature.dao;

import com.revature.models.User;

public interface UserDao {
    User insertUser(User user);
    User getOneUser(String username);

}
