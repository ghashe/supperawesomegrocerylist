package com.revature.dao;

import com.revature.models.GroceryList;

import java.util.List;

public interface GroceryListDao {
    void deleteListGivenListId(Integer listId);
    List<GroceryList> getAllListsGivenUserId(Integer userId);
    void createList(GroceryList list);



}
